/*
 * Copyright 2015 FirstBest Systems Inc.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of FirstBest Systems Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with FirstBest.
 */

package com.test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author FirstBest Systems Inc.
 */
public class Test {
    public static String format(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setLenient(false);
        return format.format(date);
    }
    
    public static void main(String[] args) {
        System.out.println(Test.format(new Date(), "mm/dd/yyyy"));
    }
    }


